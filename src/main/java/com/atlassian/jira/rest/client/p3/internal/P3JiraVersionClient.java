/*
 * Copyright (C) 2011 Atlassian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.jira.rest.client.p3.internal;

import com.atlassian.jira.rest.client.ProgressMonitor;
import com.atlassian.jira.rest.client.VersionRestClient;
import com.atlassian.jira.rest.client.domain.Version;
import com.atlassian.jira.rest.client.domain.VersionRelatedIssuesCount;
import com.atlassian.jira.rest.client.domain.input.VersionInput;
import com.atlassian.jira.rest.client.domain.input.VersionPosition;
import com.atlassian.jira.rest.client.internal.jersey.AbstractJerseyRestClient;
import com.atlassian.jira.rest.client.internal.json.JsonObjectParser;
import com.atlassian.jira.rest.client.internal.json.VersionJsonParser;
import com.atlassian.jira.rest.client.internal.json.VersionRelatedIssueCountJsonParser;
import com.atlassian.jira.rest.client.internal.json.gen.JsonGenerator;
import com.atlassian.jira.rest.client.internal.json.gen.VersionInputJsonGenerator;
import com.atlassian.jira.rest.client.internal.json.gen.VersionPositionInputGenerator;
import com.atlassian.jira.rest.client.p3.JiraVersionClient;
import com.atlassian.labs.remoteapps.api.Promise;
import com.atlassian.labs.remoteapps.api.service.http.HostHttpClient;
import com.sun.jersey.client.apache.ApacheHttpClient;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import javax.annotation.Nullable;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

/**
 * Jersey-based implementation of VersionRestClient
 *
 * @since client 0.3, server 4.4
 */
public class P3JiraVersionClient extends AbstractP3RestClient implements JiraVersionClient
{
	private final URI versionRootUri;

	public P3JiraVersionClient(HostHttpClient client) {
		super(client);
		versionRootUri = UriBuilder.fromUri(baseUri).path("version").build();
	}

	@Override
	public Promise<Version> createVersion(final VersionInput version) {
        return callAndParse(client.newRequest(versionRootUri).setEntity(
                toEntity(new VersionInputJsonGenerator(), version)).post(), new VersionJsonParser());
	}

	@Override
	public Promise<Version> updateVersion(URI versionUri, final VersionInput version) {
		return callAndParse(client.newRequest(versionUri).setEntity(
                toEntity(new VersionInputJsonGenerator(), version)).put(), new VersionJsonParser());
	}

	@Override
	public Promise<Void> removeVersion(URI versionUri, @Nullable URI moveFixIssuesToVersionUri,
			@Nullable URI moveAffectedIssuesToVersionUri) {
		final UriBuilder uriBuilder = UriBuilder.fromUri(versionUri);
		if (moveFixIssuesToVersionUri != null) {
			uriBuilder.queryParam("moveFixIssuesTo", moveFixIssuesToVersionUri);
		}
		if (moveAffectedIssuesToVersionUri != null) {
			uriBuilder.queryParam("moveAffectedIssuesTo", moveAffectedIssuesToVersionUri);
		}
        return call(client.newRequest(uriBuilder.build()).delete());
	}

	@Override
	public Promise<Version> getVersion(URI versionUri) {
        return callAndParse(client.newRequest(versionUri).get(), new VersionJsonParser());
	}

	@Override
	public Promise<VersionRelatedIssuesCount> getVersionRelatedIssuesCount(URI versionUri) {
		final URI relatedIssueCountsUri = UriBuilder.fromUri(versionUri).path("relatedIssueCounts").build();
        return callAndParse(client.newRequest(relatedIssueCountsUri).get(), new VersionRelatedIssueCountJsonParser());
	}

	@Override
	public Promise<Integer> getNumUnresolvedIssues(URI versionUri) {
		final URI unresolvedIssueCountUri = UriBuilder.fromUri(versionUri).path("unresolvedIssueCount").build();
        return callAndParse(client.newRequest(unresolvedIssueCountUri).get(), new JsonObjectParser<Integer>() {
			@Override
			public Integer parse(JSONObject json) throws JSONException {
				return json.getInt("issuesUnresolvedCount");
			}
		});
	}

	@Override
	public Promise<Version> moveVersionAfter(URI versionUri, URI afterVersionUri) {
		final URI moveUri = getMoveVersionUri(versionUri);

        return callAndParse(client.newRequest(moveUri).setEntity(toEntity(new JsonGenerator<URI>() {
			@Override
			public JSONObject generate(URI uri) throws JSONException {
				final JSONObject res = new JSONObject();
				res.put("after", uri);
				return res;
			}
		}, afterVersionUri)).post(), new VersionJsonParser());
	}

	@Override
	public Promise<Version> moveVersion(URI versionUri, final VersionPosition versionPosition) {
		final URI moveUri = getMoveVersionUri(versionUri);
        return callAndParse(client.newRequest(moveUri).setEntity(toEntity(new VersionPositionInputGenerator(), versionPosition)).post(),
				new VersionJsonParser());
	}

	private URI getMoveVersionUri(URI versionUri) {
		return UriBuilder.fromUri(versionUri).path("move").build();
	}
}

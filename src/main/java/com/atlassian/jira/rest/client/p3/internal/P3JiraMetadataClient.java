/*
 * Copyright (C) 2010 Atlassian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.jira.rest.client.p3.internal;

import com.atlassian.jira.rest.client.MetadataRestClient;
import com.atlassian.jira.rest.client.ProgressMonitor;
import com.atlassian.jira.rest.client.domain.*;
import com.atlassian.jira.rest.client.internal.jersey.AbstractJerseyRestClient;
import com.atlassian.jira.rest.client.internal.json.*;
import com.atlassian.jira.rest.client.p3.JiraMetadataClient;
import com.atlassian.labs.remoteapps.api.Promise;
import com.atlassian.labs.remoteapps.api.service.http.HostHttpClient;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.client.apache.ApacheHttpClient;
import org.codehaus.jettison.json.JSONObject;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.concurrent.Callable;

/**
 * Jersey-based implementation of MetadataRestClient
 *
 * @since v0.1
 */
public class P3JiraMetadataClient extends AbstractP3RestClient implements JiraMetadataClient
{
	private final String SERVER_INFO_RESOURCE = "/serverInfo";
	private final ServerInfoJsonParser serverInfoJsonParser = new ServerInfoJsonParser();
	private final IssueTypeJsonParser issueTypeJsonParser = new IssueTypeJsonParser();
	private final GenericJsonArrayParser<IssueType> issueTypesJsonParser = GenericJsonArrayParser.create(issueTypeJsonParser);
	private final StatusJsonParser statusJsonParser = new StatusJsonParser();
	private final PriorityJsonParser priorityJsonParser = new PriorityJsonParser();
	private final GenericJsonArrayParser<Priority> prioritiesJsonParser = GenericJsonArrayParser.create(priorityJsonParser);
	private final ResolutionJsonParser resolutionJsonParser = new ResolutionJsonParser();
	private final GenericJsonArrayParser<Resolution> resolutionsJsonParser = GenericJsonArrayParser.create(resolutionJsonParser);
	private final IssueLinkTypesJsonParser issueLinkTypesJsonParser = new IssueLinkTypesJsonParser();

	public P3JiraMetadataClient(HostHttpClient client) {
		super(client);
	}

	@Override
	public Promise<IssueType> getIssueType(final URI uri) {
		return callAndParse(client.newRequest(uri).get(), issueTypeJsonParser);
	}

	@Override
	public Promise<Iterable<IssueType>> getIssueTypes() {
		final URI uri = UriBuilder.fromUri(baseUri).path("issuetype").build();
        return callAndParse(client.newRequest(uri).get(), issueTypesJsonParser);
	}

	@Override
	public Promise<Iterable<IssuelinksType>> getIssueLinkTypes() {
		final URI uri = UriBuilder.fromUri(baseUri).path("issueLinkType").build();
        return callAndParse(client.newRequest(uri).get(), issueLinkTypesJsonParser);
	}

	@Override
	public Promise<Status> getStatus(final URI uri) {
        return callAndParse(client.newRequest(uri).get(), statusJsonParser);
	}

	@Override
	public Promise<Priority> getPriority(final URI uri) {
        return callAndParse(client.newRequest(uri).get(), priorityJsonParser);
	}

	@Override
	public Promise<Iterable<Priority>> getPriorities() {
		final URI uri = UriBuilder.fromUri(baseUri).path("priority").build();
        return callAndParse(client.newRequest(uri).get(), prioritiesJsonParser);
	}

	@Override
	public Promise<Resolution> getResolution(URI uri) {
        return callAndParse(client.newRequest(uri).get(), resolutionJsonParser);
	}

	@Override
	public Promise<Iterable<Resolution>> getResolutions() {
		final URI uri = UriBuilder.fromUri(baseUri).path("resolution").build();
        return callAndParse(client.newRequest(uri).get(), resolutionsJsonParser);
	}

	@Override
	public Promise<ServerInfo> getServerInfo() {
        return callAndParse(client.newRequest(UriBuilder.fromUri(baseUri)
                .path(SERVER_INFO_RESOURCE).build()).get(), serverInfoJsonParser);
	}
}
